### 1. Clone project
```
git clone git@gitlab.com:haitham.sweilem/buyme.git
```
### 2. Checkout the 'dev' branch
```
git checkout dev
```
### 3. Install Composer dependencies
```
composer install
```
### 4. Setup MySQL
  1. The application uses 'root' access to local MySQL server with no password
  2. Create a 'test' database:
```
  mysql> create database test;
```
  3. Run migration scripts:
  ```
  php artisan migrate
  php artisan db:seed
  ```

### 5. Run the application on a local web server
```
php artisan serve
```
### License

[MIT license](http://opensource.org/licenses/MIT)
