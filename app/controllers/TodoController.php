<?php

class TodoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET	/resource	index	resource.index
	 *
	 * @return Response
	 */
	public function index()
	{
		return "Hello :P";
	}


	/**
	 * Show the form for creating a new resource.
	 * GET	/resource/create	create	resource.create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 * POST	/resource	store	resource.store
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 * GET	/resource/{resource}	show	resource.show
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 * GET	/resource/{resource}/edit	edit	resource.edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 * PUT/PATCH	/resource/{resource}	update	resource.update
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 * DELETE	/resource/{resource}	destroy	resource.destroy
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
